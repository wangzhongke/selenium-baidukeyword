import random
import re

#查询睡眠时间范围
BAIDU_SLEEP_TIME_START = 5
BAIDU_SLEEP_TIME_END = 10

TARGER_SLEEP_TIME_START = 5
TARGER_SLEEP_TIME_END = 10

#需要查找的关键词文件路径
KEYWORDS_LIST_FILE = "./keywords_list.txt"

# 请求头
USER_AGENT = [
    'User-Agent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_0) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11"',
    'User-Agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36"',
    'User-Agent="Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.41 Safari/535.1 QQBrowser/6.9.11079.201"',
    'User-Agent="Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.3 (KHTML, like Gecko) Chrome/6.0.472.33 Safari/534.3 SE 2.X MetaSr 1.0"',
    'User-Agent="Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.835.163 Safari/535.1"',
]

HEADER = {
    'User-Agent':random.choice(USER_AGENT),
}

#是否开启IP代理
IP_PROXY_STATUS = False

#需要排除（不点击）的网站域名，只需要填写中间域名
#如www.baidu.com 只需要填写baidu
EXCLUDE_DOMAIN = "taobao"

#需要排除（不点击）的公司名称
EXCLUDE_COMPANY = "淘宝"

#匹配网址或者公司名称的xpath
AD_DOMAIN_XPATH = "//div[contains(@id,'300')]/div//span[contains(text(),'20')]/../span[1]/text()|//div[contains(@id,'600')]/div//span[contains(text(),'20')]/../span[1]/text()"

#代理文件路径，每行一个代理
PROXY_FILE = "./proxy.txt"

#获取首页广告原地址标题的xpath
AD_TITLE_URLS_XPATH = '//div[contains(@id,"300")]//h3/a/@href|//div[contains(@id,"600")]//h3/a/@href'

AD_DOMAIN_RE = re.compile('<div id="[36]00.".*?<a hidefocus.*?</a></div><div class=.*?><a href.*?<span.*?>(.*?)</span>&nbsp;<span class=.*?>20',re.S)

AD_TITLE_URLS_RE = re.compile('<div id="[36]00.".*?<h3.*?<a href="(.*?)" class',re.S)

AD_TITLE_RE = re.compile('<div id="[36]00.".*?<h3.*?<a.*?>[\u4e00-\u9fa5].*?</a>',re.S)