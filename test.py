import re

with open('test.html','r') as f:
    html = f.read()


AD_DOMAIN_RE = re.compile('<div id="[36]00.".*?<a hidefocus.*?</a></div><div class=.*?><a href.*?<span.*?>(.*?)</span>&nbsp;<span class=.*?>20',re.S)

AD_TITLE_URLS_RE = re.compile('<div id="[36]00.".*?<h3.*?<a href="(.*?)" class',re.S)

AD_TITLE_RE = re.compile('<div id="[36]00.".*?<h3.*?<a.*?>[\u4e00-\u9fa5].*?</a>',re.S)

print(re.findall(AD_DOMAIN_RE,html))