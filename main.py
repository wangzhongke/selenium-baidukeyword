# @Time : 2020年11月14日
# @Author : _黎先生

'''
用于在百度上搜索关键词，并进行点击刷流量，动态更改User-Agent
可添加代理，代理文件请以json格式放到当前目录，命名为proxy.json
'''

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from lxml import etree
import time
import random
from conf import *
import requests
import re

PROXY_LIST = []

KEYWORDS_LIST = []

class BaiDu_Flow():
    def __init__(self,key_word,proxy_ip = None,*args,**kwargs):

        # 改变配置文件的代理.
        # self.option.add_argument("--proxy-server=http://{}".format(proxy_ip))

        #当前搜索的关键词
        self.key_word = key_word

        self.url = "http://www.baidu.com/s?wd={}".format(self.key_word)
        # self.url = "https://www.baidu.com/s?wd=ip"

        self.proxy_ip = proxy_ip

        #实例option用于修改浏览器配置，如请求头、IP代理等
        self.option = webdriver.ChromeOptions()

        #添加随机请求头
        self.option.add_argument(random.choice(USER_AGENT))

        if proxy_ip != None:
            print("已启动代理模式，正在使用【{}】地址就行访问".format(self.proxy_ip))

            self.option.add_argument('--proxy-server=http://{}'.format(self.proxy_ip))

        #创建Chrome浏览器
        self.driver = webdriver.Chrome('./chromedriver',options=self.option)

        #标识循环状态
        self.flag = True

    #请求url
    def request(self):

        #请求url
        self.driver.get(self.url)

        #用xpath解析页面并得到url
        self.get_ad_url()

    #如果当前页面没有匹配到网页，会执行该方法到下一页
    def get_ad_url(self):

        # 获取当前html
        page_source = self.driver.page_source

        # #把页面源码转成lmxl
        # html = etree.HTML(page_source)
        #
        # #xpath获取广告的url，并放入列表
        # self.ad_urls_list = html.xpath(AD_TITLE_URLS_XPATH)

        #xpath有时候会匹配到标题后到 名企字段，这里把这个字段去除
        # self.ad_urls_list = [i for i in self.ad_urls_list if i != "名企"]

        #xpath获取广告对应的网址或公司名称，进行筛选匹配
        # self.ad_domain_list = html.xpath(AD_DOMAIN_XPATH)

        ######################################### 正则 #########################################
        self.ad_urls_list = re.findall(AD_TITLE_URLS_RE, page_source)
        print("广告地址：{}".format(self.ad_urls_list))

        self.ad_title_list = re.findall(AD_TITLE_RE, page_source)
        print("广告标题：{}".format(self.ad_title_list))

        self.ad_domain_list = re.findall(AD_DOMAIN_RE,page_source)
        print("域名：{}".format(self.ad_domain_list))

        ad_info = {}

        #把广告domain和url放到字典里
        for url, domain,title in zip(self.ad_urls_list, self.ad_domain_list, self.ad_title_list):
            ad_info[url] = [title,domain]

        print("广告整合字典：",ad_info)

        print("【{}】已找到【{}】个广告位".format(self.key_word,len(self.ad_urls_list)))

        #遍历字典获取domain和url，对domain进行筛选，把排除（不点击）的公司不进行访问
        for url in ad_info:

            if ad_info[url][1].find(EXCLUDE_DOMAIN) != -1 or ad_info[url][1].find(EXCLUDE_COMPANY) != -1:
                self.request_ad_url(url)
                print("{}已点击，：UR：{}".format(ad_info[url][0],url))
            else:
                print("{}被跳过点击。".format(EXCLUDE_DOMAIN if EXCLUDE_DOMAIN else EXCLUDE_COMPANY))

        self.flag = False

    #用requests请求AD广告地址
    def request_ad_url(self,ad_url):
        #如果有代理
        if self.proxy_ip != None:
            proxies = {
                "http": "http://{}/".format(self.proxy_ip),
                "https": "https://{}/".format(self.proxy_ip)
            }

            self.resp = requests.get(ad_url,HEADER,proxies=proxies)
        else:
            self.resp = requests.get(ad_url, HEADER)

        if self.resp.status_code == 200:
            print("点击成功！")
            print(self.resp.url)
        else:
            print("点击失败！")

    def run(self):
        try:
            print("正在查找关键词【{}】".format(self.key_word))

            while self.flag:
                self.request()
                time.sleep(60)
        # except Exception as e:
        #     print("发生问题：%s" %e)
        finally:

            self.driver.quit()

if __name__ == '__main__':


    if IP_PROXY_STATUS:
        # 打开IP代理JSON文件
        with open(PROXY_FILE, 'r', encoding="UTF-8") as f:

            for line in f:
                PROXY_LIST.append(line.strip())

    #打开关键词文件并读取每行关键词
    with open(KEYWORDS_LIST_FILE,'r',encoding="utf-8") as f:
        for line in f:
            KEYWORDS_LIST.append(line.strip())


    if IP_PROXY_STATUS:
        for proxy in PROXY_LIST:
            for key in KEYWORDS_LIST:
                #实例化
                demo = BaiDu_Flow(key,proxy)

                #运行
                demo.run()
    else:
        for key in KEYWORDS_LIST:
            # 实例化
            demo = BaiDu_Flow(key)

            # 运行
            demo.run()