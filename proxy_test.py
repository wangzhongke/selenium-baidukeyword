# @Time : 2020-11-19 17:07
# @Author : _黎先生
# @File : proxy_test.py

'''
测试代理ip是否可用
'''
import time
from selenium import webdriver
from selenium.webdriver import ChromeOptions

PROXY_LIST = []

url = "http://www.baidu.com/s?wd=ip"

with open("proxy.txt",'r',encoding="utf-8") as f:
    for i in f.readlines():
        PROXY_LIST.append(i.strip())

print(PROXY_LIST)


for i in PROXY_LIST:
    option = ChromeOptions()

    option.add_argument("--proxy-server=http://{}".format(i))

    driver = webdriver.Chrome(options=option)

    driver.get(url)
    print("当前代理IP为：{}".format(i))

    time.sleep(5)

    driver.quit()

